#!/bin/bash
echo ">>> STARTING INIT"
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
#sudo apt-get install -y postfix
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo GITLAB_ROOT_PASSWORD=${root_password} EXTERNAL_URL="https://${public_ip}" apt-get install gitlab-ee
