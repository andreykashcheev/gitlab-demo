provider "aws" {
  region = "${var.AWS_DEFAULT_REGION}"
}

resource "random_string" "prefix" {
  length = 8
  upper = false
  special = false
}

resource "random_string" "rootpwd" {
  length      = 16
  min_upper   = 1
  min_lower   = 1
  min_numeric = 1
  special     = false
}

variable "global_tags" {
  type = map
  default = {
    creator = "Terraform"
  }
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = merge(var.global_tags, {Name="aws-vpc-${random_string.prefix.result}"})
}


resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.main.id}"
  tags = merge(var.global_tags, {Name="aws-internet-gateway-${random_string.prefix.result}"})
}
resource "aws_route_table" "subnet0" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gateway.id}"
  }
  tags = merge(var.global_tags, {Name="aws-route-table-mgmt-${random_string.prefix.result}"})
}

resource "aws_route_table" "subnet1" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gateway.id}"
  }
  tags = merge(var.global_tags, {Name="aws-route-table-external-${random_string.prefix.result}"})
}

resource "aws_subnet" "subnet0" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${var.AWS_DEFAULT_REGION}a"
  cidr_block = "10.0.0.0/24"
  tags = merge(var.global_tags, {Name="aws-subnet-mgmtAz1-${random_string.prefix.result}"})
}

resource "aws_route_table_association" "subnet0Az1" {
  subnet_id      = "${aws_subnet.subnet0.id}"
  route_table_id = "${aws_route_table.subnet0.id}"
}


resource "aws_security_group" "subnet0" {
  description = "subnet0 interface rules"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = 6
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = 6
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(var.global_tags, {Name="aws-security-group-subnet0-${random_string.prefix.result}"})
}


resource "aws_subnet" "subnet1" {
  vpc_id = "${aws_vpc.main.id}"
  availability_zone = "${var.AWS_DEFAULT_REGION}a"
  cidr_block = "10.0.1.0/24"
  tags = merge(var.global_tags, {Name="aws-subnet-subnet1-${random_string.prefix.result}"})
}

resource "aws_route_table_association" "subnet1" {
  subnet_id      = "${aws_subnet.subnet1.id}"
  route_table_id = "${aws_route_table.subnet1.id}"
}



resource "aws_network_interface" "subnet0" {
  subnet_id = "${aws_subnet.subnet0.id}"
  security_groups = ["${aws_security_group.subnet0.id}"]
  description = "Management Interface for BIG-IP"
  tags = merge(var.global_tags, {Name="aws-network-interface-subnet0-${random_string.prefix.result}"})
}

resource "aws_eip" "subnet0" {
  network_interface = "${aws_network_interface.subnet0.id}"
  associate_with_private_ip = "${tolist(aws_network_interface.subnet0.private_ips)[0]}"
  tags = merge(var.global_tags, {Name="aws-eip-subnet0-${random_string.prefix.result}"})
}

resource "aws_network_interface" "subnet1" {
  subnet_id = "${aws_subnet.subnet1.id}"
  description = "Public External Interface for the BIG-IP"
  private_ips_count = 1
  tags = merge(var.global_tags, {Name="aws-network-interface-subnet1-${random_string.prefix.result}"})
}

resource "aws_eip" "subnet1" {
  network_interface = "${aws_network_interface.subnet1.id}"
  associate_with_private_ip = "${tolist(aws_network_interface.subnet1.private_ips)[0]}"
  tags = merge(var.global_tags, {Name="aws-eip-subnet1-${random_string.prefix.result}"})
}


resource "aws_iam_role" "main" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  tags = merge(var.global_tags, {Name="aws-iam-role-${random_string.prefix.result}"})
}


resource "aws_iam_instance_profile" "instance_profile" {
  name = "aws-iam-instance-profile-${random_string.prefix.result}"
  role = "${aws_iam_role.main.id}"
}


data "template_file" "user_data_vm0" {
  template = "${file("${path.module}/user_data.tpl")}"
  vars = {
    root_password  = random_string.rootpwd.result,
    public_ip      = aws_eip.subnet0.public_ip,
    packages       = var.gitlab_packages
  }
}

resource "aws_instance" "vm0" {
  ami = "${var.vm_image_id}"
  instance_type = "c5.xlarge"
  availability_zone = "${var.AWS_DEFAULT_REGION}a"
  key_name = "demo"
  network_interface {
    network_interface_id = "${aws_network_interface.subnet0.id}"
    device_index = 0
  }
  network_interface {
    network_interface_id = "${aws_network_interface.subnet1.id}"
    device_index = 1
  }
  iam_instance_profile = "${aws_iam_instance_profile.instance_profile.name}"
  user_data = data.template_file.user_data_vm0.rendered
  tags = merge(var.global_tags, {Name="vm0-${random_string.prefix.result}"})
}

output "deployment_info" {
  value = {
    instances: [
      {
        root_password = random_string.rootpwd.result,
        publicIp = aws_eip.subnet0.public_ip,
        instanceId = aws_instance.vm0.id
      },
    ],
    deploymentId: random_string.prefix.result,
    environment: "aws",
    region: var.AWS_DEFAULT_REGION,
  }
}
