variable "AWS_DEFAULT_REGION" {
  default     = "us-west-2"
}

variable "admin_username" {
  description = "admin username for the virtual machine"
  default     = "demouser"
}

variable "admin_password" {
  description = "admin user password"
  default     = "Welcome123!"
}

variable "vm_image_id" {
  description = "aws machine id"
  default     = "ami-008fe2fc65df48dac"
}


variable "gitlab_packages" {
  description = "List of packages to install using cloud-init"
  type        = string
  default     = "openssh-server, ca-certificates, tzdata, perl"
}
